let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Copy Vendor JS
mix.scripts('node_modules/jquery/dist/jquery.js', 'public/assets/js/vendor/jquery.js')
    .scripts('node_modules/what-input/dist/what-input.js', 'public/assets/js/vendor/what-input.js')
    .scripts(['node_modules/foundation-sites/dist/js/foundation.js', 'node_modules/motion-ui/dist/motion-ui.js'], 'public/assets/js/vendor/foundation.js')
    .scripts('node_modules/trumbowyg/dist/trumbowyg.js', 'public/assets/js/vendor/trumbowyg/trumbowyg.js');

// Copy Vendor plugins
mix.copy('node_modules/trumbowyg/dist/plugins', 'public/assets/js/vendor/trumbowyg/plugins')
    .copy('node_modules/trumbowyg/dist/ui/icons.svg', 'public/assets/vendor/trumbowyg/icons.svg');

// Copy Vendor Fonts
mix.copy('node_modules/font-awesome/fonts', 'public/assets/fonts');

// Compress Images
mix.copy('resources/assets/img', 'public/assets/img', false);

// Project Fonts
mix.copy('resources/assets/fonts', 'public/assets/fonts');

// Copy dashboard JS
mix.scripts('resources/assets/js/rich-text.js', 'public/assets/js/rich-text.js')
    .scripts('resources/assets/js/media.js', 'public/assets/js/media.js')
    .scripts('resources/assets/js/amateur-home.js', 'public/assets/js/amateur-home.js');

// Complie Project Sass + Javascript
mix.scripts('resources/assets/js/app.js', 'public/assets/js/app.js')
    .sass('resources/assets/scss/main.scss', 'public/assets/css', {
        includePaths: [
            'node_modules/foundation-sites/scss',
            'node_modules/motion-ui/src',
            'node_modules/font-awesome/scss',
            'node_modules/trumbowyg/dist/ui/sass'
        ]
    }).options({
        processCssUrls: false,
        //uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
        postCss: [
            require('autoprefixer')(),
            require('postcss-discard-comments'),
            require('cssnano')({
                autoprefixer: false,
                discardComments: {
                    removeAll: true
                }
            }),
        ] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
    })
    //.version()
    .browserSync('amateurrocks.dev');
