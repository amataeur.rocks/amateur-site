<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'media_type_id',
        'project_id',
        'media_url',
        'external_slug',
        'order'
    ];

    // Relationships
    public function type() {
        return $this->hasOne(MediaType::class, 'id', 'media_type_id');
    }

    public function project() {
        return $this->belongsTo(Project::class);
    }
}
