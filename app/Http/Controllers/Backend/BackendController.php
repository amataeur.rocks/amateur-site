<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Section;
use App\Project;
use App\Media;

class BackendController extends Controller
{
    public function index() {
        $sections = count(Section::get());
        $projects = count(Project::get());
        $media    = count(Media::get());

        return view('backend.dashboard')->with(compact('sections', 'projects', 'media'));
    }
}
