<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Project;
use Storage;
use File;
use App\Traits\ImageTrait;

class ProjectController extends Controller
{
    use ImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::get()->sortByDesc('order');
        return view('backend.project.list')->with(compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate File
        $extension = $this->get_mime_extension(File::mimeType($request->file('cover')));

        if ($extension == 'error') {
            return back()->with(['error' => 'img']);
        }

        // Check last project for ordering
        $last_project = Project::orderByDesc('order')->first();
        if ($last_project != null) {
            $order = $last_project->order + 1;
        } else {
            $order = 1;
        }

        // Create and store new project
        $project = new Project;
        $project->name = $request->name;
        $project->description = $request->description;
        if($request->published == 'on') {
            $project->published = true;
        } else {
            $project->published = false;
        }

        $project->cover = '';
        $project->order = $order;

        $project->save();

        // Store Cover image for this project
        $cover_file = Storage::putFileAs(
            'public/covers', $request->file('cover'), $project->id.$extension
        );
        $project->cover = 'storage/covers/'.$project->id.$extension;
        $project->save();

        $response = [
            'title'    => 'Project Created',
            'action'   => 'create',
            'type'     => 'success',
            'response' => 'Project '.$project->name.' was created successfully.'
        ];

        // Return Success view
        return view('backend.project.response')->with(compact('project', 'response'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('project.edit', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);

        return view('backend.project.edit')->with(compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $project = Project::findOrFail($id);

        $project->name        = $request->name;
        $project->description = $request->description;
        if($request->published == 'on') {
            $project->published = true;
        } else {
            $project->published = false;
        }

        // Upload image if any
        if ($request->hasFile('cover')) {
            // Validate File
            $extension = $this->get_mime_extension(File::mimeType($request->file('cover')));
            if ($extension == 'error') {
                return back()->with(['error' => 'img']);
            }

            // Store Cover image for this project
            $cover_file = Storage::putFileAs(
                'public/covers', $request->file('cover'), $project->id.$extension
            );
            $project->cover = 'storage/covers/'.$project->id.$extension;
        }

        $project->save();

        $response = [
            'title'    => 'Project Updated',
            'action'   => 'update',
            'type'     => 'success',
            'response' => 'Project '.$project->name.' was updated successfully.'
        ];

        return view('backend.project.response')->with(compact('project', 'response'));
    }

    /**
     * Update projects display order
     * @param  |Request $request
     * @return \Response
     */
    public function update_order(Request $request)
    {

        foreach ($request->order as $id => $order) {
            $project = Project::findOrFail($id);
            $project->order = $order;
            $project->save();
        }

        return redirect()->route('project.index');
    }

    /**
     * Ask for deleting resource confirmation
     * @param  \Request $request
     * @return \Response
     */
    public function ask_delete($id)
    {
        $project = Project::find($id);
        $response = [
            'title'    => 'Delete Project',
            'action'   => 'ask',
            'type'     => 'success',
            'response' => 'Are you sure you want to delete project '.$project->name.'?'
        ];

        return view('backend.project.response')->with(compact('project', 'response'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::findOrFail($id);

        $response = [
            'title'    => 'Project Deleted',
            'action'   => 'delete',
            'type'     => 'success',
            'response' => 'Project '.$project->name.' was deleted successfully.'
        ];

        $project->delete();

        return view('backend.project.response')->with(compact('project', 'response'));
    }

}
