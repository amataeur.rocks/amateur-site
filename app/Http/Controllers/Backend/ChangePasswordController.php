<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

class ChangePasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Change Password Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password change requests.
    |
    */

    public function index() {

        return view('backend.user.password.change');
    }

    public function update(Request $request) {

        if ($request->password == $request->password_repeat) {

            $user = Auth::user();

            $user->password = Hash::make($request->password);
            $user->save();

            $response = [
                'title'    => 'Password Updated',
                'action'   => 'update',
                'type'     => 'success',
                'response' => 'Your password was updated successfully.'
            ];

        } else {

            $response = [
                'title'    => 'Passwords Didn\'t match',
                'action'   => 'update',
                'type'     => 'failure',
                'response' => 'Your password wasn\'t updated. Both password fields have to be the same.'
            ];

        }

        return view('backend.user.password.response')->with(compact('response'));
    }
}
