<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\MediaType;
use App\Media;
use App\Project;
use App\Traits\ImageTrait;
use Storage;
use File;

class MediaController extends Controller
{
    use ImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        $project = Project::find($project_id);
        return view('backend.media.list')->with(compact('project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($project_id)
    {
        $project = Project::find($project_id);
        $media_types = MediaType::get();
        $media   = $project->media;

        $media_groups = [];
        foreach ($media as $item) {
            if ($item->type->is_group) {
                $media_groups[$item->id] = $item;
            }
        }

        return view('backend.media.create')->with(compact('project','media_types', 'media_groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $project_id)
    {

        $media = new Media;
        $media->project_id = $project_id;

        // Set type
        $media->media_type_id = $request->type;

        switch ($media->type->code) {
            case 'image':

                $extension = $this->get_mime_extension(File::mimeType($request->file('media_file')));

                if ($extension == 'error') {
                    return back()->with(['error' => 'img']);
                }

                $media->save();

                // Store image file
                $file = Storage::putFileAs(
                    'public/media/'.$project_id, $request->file('media_file'), $media->id.$extension
                );
                $media->media_url = 'storage/media/'.$project_id.'/'.$media->id.$extension;

                break;

            case 'youtube':
            case 'vimeo':
                $slug = str_replace($media->type->external_url, '', $request->url);
                $media->external_slug = $slug;
                break;

            case 'image_slide':
                $media->name = $request->name;
                break;

            case 'link':
                $media->media_url = $request->url;
                break;
        }

        //Ordering
        if($request->group && $request->group != 0) {
            $media->group = $request->group;
            $last_media = Media::where('group', $request->group)->orderByDesc('order')->first();
            if ($last_media != null) {
                $media->order = $last_media->order + 1;
            } else {
                $media->order = 1;
            }
        } else {
            $last_media = Media::where('project_id', $project_id)->orderByDesc('order')->first();
            if ($last_media != null) {
                $media->order = $last_media->order + 1;
            } else {
                $media->order = 1;
            }
        }

        $media->save();

        $response = [
            'title'    => 'Media Item Created',
            'action'   => 'create',
            'type'     => 'success',
            'response' => 'Item was created successfully.'
        ];

        return view('backend.media.response')->with(compact('media', 'response'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($project_id, $media_id)
    {
        return redirect()->route('project.media.edit', ['project' => $project_id, 'media' => $media_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($project_id, $media_id)
    {
        $project = Project::find($project_id);
        $media = Media::find($media_id);
        $media_groups = [];
        foreach ($project->media as $item) {
            if ($item->type->is_group) {
                $media_groups[$item->id] = $item;
            }
        }

        return view('backend.media.edit')->with(compact('media', 'media_groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $project_id, $media_id)
    {

        $media = Media::find($media_id);

        if($request->group) {
            $media->group = $request->group;
        }

        // Check type
        if($media->type->is_external == 0 && $media->type->is_group == 0) {

            // Type is file
            if ($request->hasFile('media_file')) {
                $extension = $this->get_mime_extension(File::mimeType($request->file('media_file')));

                if ($extension == 'error') {
                    return back()->with(['error' => 'img']);
                }

                // Store image file
                $file = Storage::putFileAs(
                    'public/media/'.$project_id, $request->file('media_file'), $media->id.$extension
                );
                $media->media_url = 'storage/media/'.$project_id.'/'.$media->id.$extension;
            }

        } elseif($media->type->is_external == 1 && $media->type->is_group == 0) {

            // Type is link
            $slug = str_replace($media->type->external_url, '', $request->url);
            $media->external_slug = $slug;

        } elseif($media->type->is_external == 0 && $media->type->is_group == 1) {

            // Type is group
            $media->name = $request->name;

        }

        $media->save();

        $response = [
            'title'    => 'Media Item Updated',
            'action'   => 'create',
            'type'     => 'success',
            'response' => 'Item was updated successfully.'
        ];

        return view('backend.media.response')->with(compact('media', 'response'));
    }

    /**
     * Update media display order
     * @param  |Request $request
     * @return \Response
     */
    public function update_order(Request $request)
    {

        foreach ($request->order as $id => $order) {
            $media = Media::findOrFail($id);
            $media->order = $order;
            $media->save();
        }

        return redirect()->route('project.media.index', $request->project_id);
    }

    /**
     * Ask for deleting resource confirmation
     * @param  \Request $request
     * @return \Response
     */
    public function ask_delete($project_id, $media_id)
    {
        $media = Media::find($media_id);
        $response = [
            'title'    => 'Delete Media',
            'action'   => 'ask',
            'type'     => 'success',
            'response' => 'Are you sure you want to delete this media item?'
        ];

        return view('backend.media.response')->with(compact('media', 'response'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($project_id, $media_id)
    {
        $media = Media::findOrFail($media_id);

        $response = [
            'title'    => 'Media Item Deleted',
            'action'   => 'delete',
            'type'     => 'success',
            'response' => 'Item was deleted successfully.'
        ];

        $media->delete();

        return view('backend.media.response')->with(compact('media', 'response'));
    }

}
