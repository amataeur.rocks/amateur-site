<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use App\Project;

class SiteController extends Controller
{
    public function index() {
        $about = Section::where('name', 'about')->first();
        $projects = Project::where('published', true)->get()->sortByDesc('order');
        $contact = Section::where('name', 'contact')->first();
        $get_high = Section::where('name', 'get_high')->first();
        return view('home')->with(compact('about', 'projects', 'contact', 'get_high'));
    }

    public function get_project_media(Request $request) {
        $project = Project::findOrFail($request->project_id);

        return view('partials.project-media')->with('media', $project->media);
    }
}
