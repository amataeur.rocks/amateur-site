<?php

namespace App\Traits;

trait ImageTrait
{
    public function get_mime_extension($mimeType){
        switch ($mimeType) {
            case 'image/jpeg':
                $extension = '.jpg';
                break;

            case 'image/png':
                $extension = '.png';
                break;

            case 'image/gif':
                $extension = '.gif';
                break;

            default:
                $extension = 'error';
                break;
        }

        return $extension;
    }
}
