<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name',
        'description',
        'cover',
        'published',
        'order'
    ];

    public function media() {
        return $this->hasMany('App\Media')->orderBy('order');;
    }
}
