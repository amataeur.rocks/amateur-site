<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Amateur(dot)rocks</title>
        <meta name="_token" content="{{ csrf_token() }}"/>
        <link rel="stylesheet" href="{{asset('/assets/css/main.css')}}">
    </head>
    <body id="amateur">

        <div class="site-container">
            <div class="grid-x margin-top">

                <div class="cell small-12 medium-10">

                    <div class="content-container">
                        @yield('content')
                    </div>

                </div>
                <div class="cell show-for-medium medium-2" data-sticky-container>
                    <div class="sticky" data-sticky data-margin-top="2">
                        <ul class="no-bullet floating-menu" data-smooth-scroll data-animation-duration="350" data-animation-easing="swing">
                            @include('partials.menu')
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <script src="{{asset('/assets/js/vendor/jquery.js')}}"></script>
        <script src="{{asset('/assets/js/vendor/what-input.js')}}"></script>
        <script src="{{asset('/assets/js/vendor/foundation.js')}}"></script>
        <script src="{{asset('/assets/js/app.js')}}"></script>
        @yield('javascript')
    </body>
</html>
