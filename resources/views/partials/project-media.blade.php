@foreach ($media as $item)
    @if (!$item->type->is_group)
        @if ($item->group == 0)
            <div class="media-item">
                @if ($item->type->code == 'image')
                    <img src="{{asset($item->media_url)}}">
                @elseif ($item->type->code == 'youtube')
                    <iframe class="video-frame" src="https://www.youtube-nocookie.com/embed/{{$item->external_slug}}" frameborder="0" allowfullscreen></iframe>
                @elseif ($item->type->code == 'vimeo')
                    <iframe class="video-frame" src="https://player.vimeo.com/video/{{$item->external_slug}}?title=0&byline=0&portrait=0" frameborder="0" allowfullscreen></iframe>
                @elseif ($item->type->code == 'link')
                    <p><a href="{{$item->media_url}}" target="_blank">{{$item->media_url}}</a></p>
                @endif
            </div>
        @endif
    @else
        <div class="media-group">

            <div class="orbit" role="region" data-orbit data-bullets="false" data-timer-delay="60000" data-use-m-u-i="false">
                <div class="orbit-wrapper">
                    <div class="orbit-controls">
                        <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span></button>
                        <button class="orbit-next"><span class="show-for-sr">Next Slide</span></button>
                    </div>
                    <ul class="orbit-container">
                        @php
                            $first = true;
                        @endphp

                        @foreach ($media as $group_item)

                            @if ($group_item->group == $item->id)

                                <li class="orbit-slide @if($first == true) is-active @endif">
                                    <figure class="orbit-figure">
                                        @if ($group_item->type->code == 'youtube')
                                            <iframe class="video-frame" src="https://www.youtube-nocookie.com/embed/{{$item->external_slug}}" frameborder="0" allowfullscreen></iframe>
                                        @else
                                            <img src="{{asset($group_item->media_url)}}" class="orbit-image">
                                        @endif

                                    </figure>
                                </li>

                                @php
                                    $first = false;
                                @endphp

                            @endif

                        @endforeach

                    </ul>
                </div>
            </div>

        </div>
    @endif
@endforeach
