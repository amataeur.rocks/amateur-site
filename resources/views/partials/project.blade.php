<div class="project-header" data-id="{{$project->id}}">
    <p id="toggler-{{$project->id}}" class="toggler-container">
        <a class="project-toggle" data-id="{{$project->id}}" id="link-{{$project->id}}">{{$project->name}}</a>
    </p>
    <img src="{{$project->cover}}" alt="{{$project->name}}" class="project-preview hidden" id="preview-{{$project->id}}">
</div>


<div class="project hidden" id="project-{{$project->id}}">

    {!! $project->description !!}

    <div class="media-container">
        {{-- dinamically Filled --}}
    </div>

    <p class="close-p"><a class="close-toggler" data-id="{{$project->id}}">(Close)</a></p>

</div>
