@extends('layouts.default')

@section('content')

    <section id="top">
        {!! $about->content !!}
    </section>

    <section class="margin-bottom projects" id="projects">

        @if (count($projects) > 0)

            <h1 class="works-title">Selected work &nbsp;&nbsp;&darr;</h1>

            @foreach ($projects as $project)
                @include('partials.project')
            @endforeach

        @else
            We will post our projects here soon.
        @endif

    </section>

    <section id="contact">
        {!! $contact->content !!}
    </section>

    <section>
        <p>
            @if ($get_high->content != '')
                <a href="{{$get_high->content}}" target="_blank">get</a> or
            @endif

            <a href="#top" data-smooth-scroll data-animation-duration="350">go high?</a>
            &nbsp;&nbsp;&uarr;
        </p>
    </section>
@endsection

@section('javascript')
    <script src="{{asset('/assets/js/amateur-home.js')}}"></script>
@endsection
