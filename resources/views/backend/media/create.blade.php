@extends('backend.layouts.dashboard')
@section('title', 'Add Media')

@section('content')
    <h1>Add Media</h1>
    <hr>

    <form action="{{route('project.media.store', $project->id)}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}

        <label>Type
            <select name="type" id="type">
                <option value="null">-</option>
                <optgroup label="Choose one media type">
                    @foreach ($media_types as $type)
                        <option value="{{$type->id}}">{{$type->name}}@if($type->is_group) (Group of items)@endif</option>
                    @endforeach
                </optgroup>
            </select>
        </label>


        @if (count($media_groups) != 0)
            <label id="media_group_label" class="hidden">Group
                <select name="group">
                    <option value="0">None (independent item)</option>
                    @foreach ($media_groups as $group)
                        <option value="{{$group->id}}">{{$group->name}}</option>
                    @endforeach
                </select>
            </label>
        @endif

        <label id="media_file_label" class="hidden">Upload File
            <input type="file" name="media_file" id="media_file_input">
        </label>

        <label id="media_external_url_label" class="hidden">Media External URL
            <input type="text" name="url" id="media_external_url_input">
        </label>

        <label id="media_group_name_label" class="hidden">Group Reference Name (for project manager)
            <input type="text" name="name" id="media_group_name_input">
        </label>

        <input id="submit_button" type="submit" class="button button-default hidden" name="submit" value="Create">
    </form>
@endsection

@section('javascript')

    <script>
        var mediaType = [];

        @foreach ($media_types as $type)
            mediaType[{{$type->id}}] = [];
            mediaType[{{$type->id}}]['code']         = '{{$type->code}}';
            mediaType[{{$type->id}}]['name']         = '{{$type->name}}';
            mediaType[{{$type->id}}]['is_external']  = '{{$type->is_external}}';
            mediaType[{{$type->id}}]['external_url'] = '{{$type->external_url}}';
            mediaType[{{$type->id}}]['is_group']     = '{{$type->is_group}}';
        @endforeach

    </script>
    <script src="{{asset('assets/js/media.js')}}"></script>

@endsection
