@extends('backend.layouts.dashboard')
@section('title', 'Project Media')

@section('content')

    <h1>{{$project->name}} Media</h1>
    <hr>

    @if (count($project->media) !== 0)

        <div class="grid-x">
            <div class="cell auto text-right">
                <a href="{{route('project.media.create', $project->id)}}" class="button clear primary"><i class='fa fa-plus'></i> Add Media</a>
            </div>
        </div>

        <form action="{{route('project.media.update_order', $project->id)}}" method="post">

            {{csrf_field()}}
            <input type="hidden" name="project_id" value="{{$project->id}}">

            <table class="table-hover text-center">
                <tr>
                    <th>Order</th>
                    <th>Group Order</th>
                    <th>Type</th>
                    <th>Thumbnail / URL / Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>

                @foreach ($project->media as $item)
                    @if ($item->group == 0)
                        <tr>
                            <td>
                                <input type="number" name="order[{{$item->id}}]" value="{{$item->order}}" class="order-input">
                            </td>
                            <td></td>
                            <td>{{App\Media::find($item->id)->type->name}} @if($item->type->is_group)(Group)@endif</td>
                            <td>
                                @if ($item->type->code == 'image')
                                    <img src="{{asset($item->media_url)}}" class="thumbnail">
                                @elseif ($item->type->code == 'youtube' || $item->type->code == 'vimeo')
                                    <a href="{{$item->type->external_url}}{{$item->external_slug}}" target="_blank">
                                        {{$item->type->external_url}}{{$item->external_slug}}
                                    </a>
                                @elseif ($item->type->code == 'link')
                                    <a href="{{$item->media_url}}" target="_blank">
                                        {{$item->media_url}}
                                    </a>
                                @elseif ($item->type->code == 'image_slide')
                                    Group: {{$item->name}}
                                @endif

                            </td>
                            <td><a href="{{route('project.media.edit', ['project' => $project->id, 'media' => $item->id])}}"><i class="fa fa-pencil"></i></a></td>
                            <td><a href="{{route('project.media.ask_delete', ['project' => $project->id, 'media' => $item->id])}}"><i class='fa fa-trash'></i></a></td>
                        </tr>
                    @endif
                    @if ($item->type->is_group)

                        @foreach ($project->media as $subitem)
                            @if ($subitem->group == $item->id)
                                <tr>
                                    <td><i class='fa fa-chevron-right'></i></td>
                                    <td>
                                        <input type="number" name="order[{{$subitem->id}}]" value="{{$subitem->order}}" class="order-input">
                                    </td>
                                    <td>{{App\Media::find($subitem->id)->type->name}} @if($subitem->type->is_group)(Group)@endif</td>
                                    <td>
                                        @if ($subitem->type->is_external)
                                            <a href="{{$subitem->type->external_url}}{{$subitem->external_slug}}" target="_blank">
                                                {{$subitem->type->external_url}}{{$subitem->external_slug}}
                                            </a>
                                        @elseif($subitem->type->is_group)
                                            Group: {{$item->name}}
                                        @else
                                            <img src="{{asset($subitem->media_url)}}" class="thumbnail">
                                        @endif
                                    </td>
                                    <td><a href="{{route('project.media.edit', ['project' => $project->id, 'media' => $subitem->id])}}"><i class="fa fa-pencil"></i></a></td>
                                    <td><a href="{{route('project.media.ask_delete', ['project' => $project->id, 'media' => $subitem->id])}}"><i class='fa fa-trash'></i></a></td>
                                </tr>
                            @endif
                        @endforeach

                    @endif
                @endforeach
                @if (count($project->media) > 1)
                    <tr>
                        <td colspan="2"><button type="submit" class="button expanded"><i class='fa fa-floppy-o'></i> Save media order</button></td>
                        <td colspan="4"></td>
                    </tr>
                @endif

            </table>

        </form>

    @else

        <p>This projects has no media yet. <a href="{{route('project.media.create', $project->id)}}">Add some</a></p>

    @endif

@endsection
