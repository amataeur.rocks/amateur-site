@extends('backend.layouts.dashboard')
@section('title', 'Edit Media')

@section('content')
    <h1>Edit Media</h1>
    <hr>

    <form action="{{route('project.media.update', ['project' => $media->project->id, 'media' => $media->id])}}" method="post" enctype="multipart/form-data">

        {{csrf_field()}}
        {{method_field('patch')}}

        <input type="hidden" name="type" value="{{$media->type->id}}">
        <h2>{{$media->type->name}}</h2>

        @if ($media->type->is_external == 0 && $media->type->is_group == 1)

            <label id="media_group_name_label">Group Reference Name (for project manager)
                <input type="text" name="name" id="media_group_name_input" value="{{$media->name}}" required>
            </label>

        @else

            @if (count($media_groups) != 0)
                <label id="media_group_label">Group
                    <select name="group">
                        <option value="0">None (independent item)</option>
                        @foreach ($media_groups as $group)
                            <option value="{{$group->id}}" @if($media->group == $group->id) selected @endif>{{$group->name}}</option>
                        @endforeach
                    </select>
                </label>
            @endif

            @if ($media->type->is_external == 0 && $media->type->is_group == 0)

                <div class="grid-x grid-margin-x">
                    <div class="cell medium-5 large-3">
                        Current Image
                        <img src="{{asset($media->media_url)}}" alt="">
                    </div>
                    <div class="cell medium-7 large-9 flex-container align-middle">
                        <div class="grid-y">
                            <div class="cell">
                                <label>Change Image</label>
                            </div>
                            <div class="cell">
                                <input type="file" name="media_file">
                            </div>
                        </div>
                    </div>
                </div>

            @elseif ($media->type->is_external == 1 && $media->type->is_group == 0)

                <label id="media_external_url_label">Media External URL
                    <input type="text" name="url" id="media_external_url_input" value="{{$media->type->external_url.$media->external_slug}}">
                </label>

            @endif

        @endif

        <button type="submit" class="button button-default margin-top" name="submit"><i class='fa fa-floppy-o'></i> Save</button>

    </form>

@endsection
