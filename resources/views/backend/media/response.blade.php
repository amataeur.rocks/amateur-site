@extends('backend.layouts.response')
@section('title', $response['title'])

@section('response')

    <p>{{$response['response']}}</p>

    @if ($response['action'] == 'ask')
        <form action="{{route('project.media.destroy', ['project' => $media->project->id, 'media' => $media->id])}}" method="post">
            {{csrf_field()}}
            {{method_field('delete')}}
            <a class="button alert" href="{{route('project.media.index', $media->project->id)}}">No</a>
            <button class="button success" type="submit">Yes</button>
            <a class="button" href="{{route('project.media.index', $media->project->id)}}"><i class='fa fa-list'></i> Go to media list</a>
        </form>
    @elseif ($response['action'] == 'delete')
        <a class="button" href="{{route('project.media.index', $media->project->id)}}"><i class='fa fa-list'></i> Go to media list</a>
    @else
        <a class="button" href="{{route('project.media.edit', ['project' => $media->project->id, 'media' => $media->id])}}"><i class='fa fa-pencil'></i> Edit Item</a>
        <a class="button" href="{{route('project.media.create', $media->project->id)}}"><i class='fa fa-plus'></i> Add More</a>
        <a class="button" href="{{route('project.media.index', $media->project->id)}}"><i class='fa fa-list'></i> Go to media list</a>
    @endif

@endsection
