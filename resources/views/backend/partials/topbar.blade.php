<div class="top-bar">
    <div class="top-bar-left">
        <ul class="dropdown menu">
            <li class="menu-text show-for-small-only"><button type="button" data-toggle="offCanvas"><i class='fa fa-bars'></i></button></li>
            <li class="menu-text">Dashboard | {{ config('app.name') }}</li>
        </ul>
    </div>
    <div class="top-bar-right hide-for-small-only">
        <ul class="dropdown menu" data-dropdown-menu>
            <li>
                <a href="#">User</a>
                <ul class="menu vertical">
                    <li><a href="{{ route('password.change') }}">Change password</a></li>
                    <li><a href="{{ route('logout') }}">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
