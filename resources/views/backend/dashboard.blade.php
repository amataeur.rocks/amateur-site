@extends('backend.layouts.dashboard')

@section('title', 'Dashboard')

@section('content')

    <h1>Dashboard</h1>

    <h4>Site summary</h4>

    <hr>

    <div class="grid-x grid-margin-x">
        <div class="cell auto text-center">
            <h5><a href="{{route('section.index')}}">Sections</a></h5>
            <div class="stat-number">{{$sections}}</div>
        </div>
        <div class="cell auto text-center">
            <h5><a href="{{route('project.index')}}">Projects</a></h5>
            <div class="stat-number">{{$projects}}</div>
        </div>
        <div class="cell auto text-center">
            <h5>Media Items</h5>
            <div class="stat-number">{{$media}}</div>
        </div>
    </div>

@endsection
