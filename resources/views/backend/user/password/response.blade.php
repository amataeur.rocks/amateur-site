@extends('backend.layouts.response')

@section('response')

    <p>{{$response['response']}}</p>

    @if ($response['type'] != 'success')
        <a href="{{route('password.change')}}" class="button">Try again</a>
    @endif

@endsection
