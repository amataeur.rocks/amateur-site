@extends('backend.layouts.dashboard')

@section('title', 'Change Password')

@section('content')

    <h1>Change Password</h1>
    <hr>

    <form action="{{ route('password.update') }}" method="post">
        
        {{csrf_field()}}

        <label>New Password</label>
        <input type="password" name="password" required>

        <label>Repeat Password</label>
        <input type="password" name="password_repeat" required>

        <input type="submit" name="submit" value="Update" class="button">

    </form>

@endsection
