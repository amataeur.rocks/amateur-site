@extends('backend.layouts.dashboard')
@section('title', 'Edit Section')

@section('content')
    <h1>Edit Section</h1>
    <hr>

    <form action="{{route('section.update', $section['id'])}}" method="post">

        {{csrf_field()}}
        {{method_field('patch')}}

        @if (!$section['is_link'])
            <label>Title</label>
            <input type="text" name="title" value="{{$section['title']}}" required>
            <label>Content</label>
            <textarea name="content" required>{{$section['content']}}</textarea>
        @else 
            <label>Title</label>
            <p>{{$section['title']}}</p>
            <input type="hidden" name="title" value="{{$section['title']}}" required>
            <label>Content</label>
            <input type="text" name="content" value="{{$section['content']}}" placeholder="http://">
        @endif


        <button type="submit" class="button button-default" name="submit"><i class='fa fa-floppy-o'></i> Save</button>

    </form>

@endsection

@section('javascript')
    <script src="{{asset('/assets/js/vendor/trumbowyg/trumbowyg.js')}}"></script>
    <script src="{{asset('/assets/js/rich-text.js')}}"></script>
@endsection
