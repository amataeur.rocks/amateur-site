@extends('backend.layouts.dashboard')
@section('title', 'Sections')

@section('content')

    <h1>Sections</h1>
    <hr>

    <table class="table-hover">
        <tr>
            <th>Name</th>
            <th>Title</th>
            <th>Edit</th>
        </tr>

        @foreach ($sections as $section)
            <tr class="text-center">
                <td>{{$section['name']}}</td>
                <td>{{$section['title']}}</td>
                <td><a href="{{route('section.edit', $section->id)}}"><i class="fa fa-pencil"></i></a></td>
            </tr>
        @endforeach

    </table>

@endsection
