@extends('backend.layouts.dashboard')
@section('title', $response['title'])

@section('content')
    <h1 class="@isset($response['type'])$response['type']@endisset">{{$response['title']}}</h1>
    <hr>

    <div class="@isset($response['type'])$response['type']@endisset">

        @yield('response')

    </div>
@endsection
