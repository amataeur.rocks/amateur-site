@extends('backend.layouts.basic')

@section('layout')
    <div class="off-canvas position-left" id="offCanvas" data-off-canvas data-transition="overlap">
        <div class="grid-x grid-padding-x grid-padding-y">
            <div class="cell">
                <h3>Dashboard | {{ config('app.name') }}</h3>
            </div>
        </div>
        <hr>
        <ul class="menu vertical">
            @include('backend.partials.menu')
        </ul>
    </div>

    <div class="off-canvas-content" data-off-canvas-content>
        @include('backend.partials.topbar')

        <div class="grid-x">

            <div class="cell medium-3 hide-for-small-only dashboard-menu">
                <ul class="menu vertical">
                    @include('backend.partials.menu')
                </ul>
            </div>

            <div class="cell medium-9">
                <div class="grid-x grid-padding-x grid-padding-y">
                    <div class="cell">
                        @yield('content')
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
