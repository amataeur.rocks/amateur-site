<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>@yield('title') | {{ config('app.name') }}</title>
        <link rel="stylesheet" href="{{asset('/assets/css/main.css')}}">
    </head>
    <body class="jv-dashboard" id="@yield('section')">

        @yield('layout')

        <script src="{{asset('/assets/js/vendor/jquery.js')}}"></script>
        <script src="{{asset('/assets/js/vendor/what-input.js')}}"></script>
        <script src="{{asset('/assets/js/vendor/foundation.js')}}"></script>
        <script src="{{asset('/assets/js/app.js')}}"></script>
        @yield('javascript')
    </body>
</html>
