@extends('backend.layouts.basic')

@section('title', 'Password Reset')
@section('section', 'login')

@section('layout')
<div class="grid-container">
    <div class="grid-x grid-margin-x align-center">
        <div class="cell medium-6 large-4 vertical-container">

            <div class="card">
                <div class="card-section">

                    <h4 class="text-center">Reset Password</h4>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="auth-form" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="{{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="email" class="cell medium-4 control-label">E-Mail Address</label>

                            <div class="cell medium-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="{{ $errors->has('password') ? 'has-error' : '' }}">
                            <label for="password" class="cell medium-4 control-label">Password</label>

                            <div class="cell medium-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="{{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                            <label for="password-confirm" class="cell medium-4 control-label">Confirm Password</label>
                            <div class="cell medium-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <button type="submit" class="button button-primary expanded">
                            Reset Password
                        </button>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
