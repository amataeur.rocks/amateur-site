@extends('backend.layouts.basic')

@section('title', 'Password Reset')
@section('section', 'login')

@section('layout')
<div class="grid-container">
    <div class="grid-x grid-margin-x align-center">
        <div class="cell medium-6 large-4 vertical-container">

            <div class="card">
                <div class="card-section">

                    <h4 class="text-center">Reset Password</h4>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="auth-form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="cell medium-4 control-label">E-Mail Address</label>

                            <div class="cell medium-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <button type="submit" class="button button-primary expanded">
                            Send Password Reset Link
                        </button>

                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
