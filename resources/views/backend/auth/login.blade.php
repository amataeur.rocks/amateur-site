@extends('backend.layouts.basic')

@section('title', 'Login')
@section('section', 'login')

@section('layout')

    <div class="grid-container">
        <div class="grid-x grid-margin-x align-center">
            <div class="cell medium-6 large-4 vertical-container">

                <div class="card">
                    <div class="card-section">
                        <form action="{{ route('login') }}" method="post" class="auth-form">
                            {{ csrf_field() }}
                            <h4 class="text-center">Log in with your email account</h4>

                            <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label>Email
                                    <input type="email" name="email" placeholder="you@domain.com" value="{{ old('email') }}">
                                </label>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password
                                    <input type="password" name="password" placeholder="Password">
                                </label>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                            {{-- <input id="show-password" type="checkbox"><label for="show-password">Show password</label> --}}
                            <p><input type="submit" class="button expanded" value="Log in"></input></p>
                            <p class="text-center"><a href="{{ route('password.request') }}">Forgot your password?</a></p>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
