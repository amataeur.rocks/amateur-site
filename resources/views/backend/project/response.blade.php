@extends('backend.layouts.response')


@section('response')

    <p>{{$response['response']}}</p>

    @if ($response['action'] == 'ask')

        <form action="{{route('project.destroy', $project->id)}}" method="post">

            {{csrf_field()}}
            {{method_field('delete')}}

            <a class="button alert" href="{{route('project.index')}}">No</a>

            <button class="button success" type="submit">Yes</button>
            <a class="button" href="{{route('project.index')}}"><i class='fa fa-list'></i> Go to projects list</a>

        </form>

    @elseif ($response['action'] == 'delete')

        <a class="button" href="{{route('project.index')}}"><i class='fa fa-list'></i> Go to projects list</a>

    @else

        <a class="button" href="{{route('project.edit', $project->id)}}"><i class='fa fa-pencil'></i> Edit project</a>
        <a class="button" href="{{route('project.media.index', $project->id)}}"><i class='fa fa-image'></i> Manage project media</a>
        <a class="button" href="{{route('project.index')}}"><i class='fa fa-list'></i> Go to projects list</a>

    @endif

@endsection
