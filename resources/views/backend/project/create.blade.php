@extends('backend.layouts.dashboard')
@section('title', 'New Project')

@section('content')
    <h1>New Project</h1>
    <hr>

    <form action="{{route('project.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <label>Name</label>
        <input type="text" name="name" value="{{old('name')}}" required>
        <label>Description</label>
        <textarea name="description" required>{{old('description')}}</textarea>
        <label>Cover</label>
        <input type="file" name="cover" value="{{old('cover')}}" required>
        <label>Published Status</label>
        <fieldset>
            <input type="checkbox" name="published" id="published" checked> Published
        </fieldset>

        <input type="submit" class="button button-default" name="submit" value="Create">
    </form>
@endsection

@section('javascript')
    <script src="{{asset('/assets/js/vendor/trumbowyg/trumbowyg.js')}}"></script>
    <script src="{{asset('/assets/js/rich-text.js')}}"></script>
@endsection
