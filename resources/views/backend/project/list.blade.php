@extends('backend.layouts.dashboard')
@section('title', 'Projects')

@section('content')

    <h1>Projects</h1>
    <hr>

    @if (count($projects) !== 0)

        <div class="grid-x">
            <div class="cell auto text-right">
                <a href="{{route('project.create')}}" class="button clear primary"><i class='fa fa-plus'></i> Add Project</a>
            </div>
        </div>

        <form action="{{route('project.update_order')}}" method="post">

            {{csrf_field()}}

            <table class="table-hover">
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Published</th>
                    <th>Order</th>
                    <th>Edit</th>
                    <th>Media</th>
                    <th>Delete</th>
                </tr>

                @foreach ($projects as $project)
                    <tr class="text-center">
                        <td>{{$project['id']}}</td>
                        <td>{{$project['name']}}</td>
                        <td>
                            @if ($project['published'])
                                Yes
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            <input type="number" name="order[{{$project->id}}]" value="{{$project->order}}" class="order-input">
                        </td>
                        <td><a href="{{route('project.edit', $project->id)}}"><i class="fa fa-pencil"></i></a></td>
                        <td><a href="{{route('project.media.index', $project->id)}}"><i class='fa fa-image'></i></a></td>
                        <td><a href="{{route('project.ask_delete', $project->id)}}"><i class='fa fa-trash'></i></a></td>
                    </tr>
                @endforeach
                @if (count($projects) > 1)
                    <tr>
                        <td colspan="3">Remember that project ordering is descendent. (Higher number is first).</td>
                        <td class="text-center"><button type="submit" class="button"><i class='fa fa-floppy-o'></i> Save projects order</button></td>
                        <td colspan="3"></td>
                    </tr>
                @endif
            </table>

        </form>

    @else

        <p>You haven publish any project yet. <a href="{{route('project.create')}}">Add one</a></p>

    @endif

@endsection
