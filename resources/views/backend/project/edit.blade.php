@extends('backend.layouts.dashboard')
@section('title', 'Edit Project')

@section('content')
    <h1>Edit Project</h1>
    <hr>

    <form action="{{route('project.update', $project->id)}}" method="post" enctype="multipart/form-data">

        {{csrf_field()}}
        {{method_field('patch')}}

        <label>Name</label>
        <input type="text" name="name" value="{{$project->name}}" required>
        <label>Description</label>
        <textarea name="description" required>{{$project->description}}</textarea>

        <label class="margin-top">Published Status</label>
        <fieldset>
            <input type="checkbox" name="published" id="published" @if ($project->published) checked @endif> Published
        </fieldset>

        <label class="margin-top">Cover</label>
        <hr>
        <div class="grid-x grid-margin-x">
            <div class="cell medium-5 large-3">
                Current Cover
                <img src="{{asset($project->cover)}}" alt="">
            </div>
            <div class="cell medium-7 large-9 flex-container align-middle">
                <div class="grid-y">
                    <div class="cell">
                        <label>Change Cover</label>
                    </div>
                    <div class="cell">
                        <input type="file" name="cover">
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <button type="submit" class="button button-default" name="submit"><i class='fa fa-floppy-o'></i> Save</button>
        <a class="button" href="{{route('project.media.index', $project->id)}}"><i class='fa fa-image'></i> Manage project media</a>

    </form>

@endsection

@section('javascript')
    <script src="{{asset('/assets/js/vendor/trumbowyg/trumbowyg.js')}}"></script>
    <script src="{{asset('/assets/js/rich-text.js')}}"></script>
@endsection
