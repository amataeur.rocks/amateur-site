$('#type').change(function() {

    // Reset form values
    $('#media_group_label').addClass('hidden');
    $('#media_file_label').addClass('hidden');
    $('#media_file_input').prop('required', false);
    $('#media_external_url_label').addClass('hidden');
    $('#media_external_url_input').prop('required', false);
    $('#media_group_name_label').addClass('hidden');
    $('#media_group_name_input').prop('required', false);
    $('#submit_button').addClass('hidden');

    // Get new type
    type = $(this).val();

    // Check type
    if (type !== null) {
        // Get type params
        type = mediaType[$(this).val()];

        if(type.is_group != 1) { // Check if it's a group

            $('#media_group_label').removeClass('hidden');

            switch (type.code) {
                case 'image':
                    $('#media_file_label').removeClass('hidden');
                    $('#media_file_input').prop('required', true);
                    break;
                case 'youtube':
                case 'vimeo':
                    $('#media_external_url_input').attr('placeholder', type.external_url).prop('required', true);
                    $('#media_external_url_label').removeClass('hidden');
                    break
                case 'link':
                    $('#media_external_url_input').attr('placeholder', 'http://').prop('required', true);
                    $('#media_external_url_label').removeClass('hidden');
            }
        } else { // If group
            $('#media_group_name_label').removeClass('hidden');
            $('#media_group_name_input').prop('required', true);
        }

        // Reveal Submit Button
        $('#submit_button').removeClass('hidden');
    }

})
