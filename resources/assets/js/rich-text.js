$.trumbowyg.svgPath = '/assets/vendor/trumbowyg/icons.svg';

$('textarea').trumbowyg({
    removeformatPasted: true,
    autogrow: true,
    btns: [
        ['viewHTML'],
        ['formatting'],
        'btnGrp-semantic',
        ['superscript', 'subscript'],
        ['link'],
        'btnGrp-justify',
        'btnGrp-lists',
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
    ]
});
