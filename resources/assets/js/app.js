$(document).foundation();

/* -----------------------------------------------------------------------------
* Include CSRF Token in AJAX requests
* ----------------------------------------------------------------------------*/
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});
