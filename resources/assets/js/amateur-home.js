/* -----------------------------------------------------------------------------
/ Toggle Projects
/ --------------------------------------------------------------------------- */

// Open/ Toggle Button
$('.projects .project-toggle').click(function() {
    id = $(this).attr('data-id');
    try_number = 10;
    error_code = 1;
    failure_message = "<p><b>Oops! Project images couldn't be loaded.</b> It appears to be an error with our server. Please, try again later.</p>";

    // Close all projects
    $('.project').addClass('hidden');
    $('.toggler-container').removeClass('hidden');

    // Switch Button with content
    $('.projects #toggler-' + id).addClass('hidden');
    $('.projects #project-' + id).removeClass('hidden');

    // Try to fetch media
    for (var i = 0; i < try_number; i++) {
        if(error_code == 1) {
            $.post('/project/media',
                {
                    project_id: id
                })
                .done(function(response) {
                    $('.projects #project-'+id+' .media-container').html(response).foundation();
                    error_code = 0;
                })
                .fail(function() {
                    console.log('Ajax connection failed');
                    $('.projects #project-'+id+' .media-container').html(failure_message).foundation();
                    error_code = 1;
                })
        }
    }

});

// Close Button
$('.projects .close-toggler').click(function() {
    id = $(this).attr('data-id');

    close_project(id);
});

function close_project(id) {
    $('.projects #project-'+id).addClass('hidden');
    $('.projects #toggler-'+id).removeClass('hidden');
}

/* -----------------------------------------------------------------------------
/ Mobile Menu
/ --------------------------------------------------------------------------- */
// $('#mobileMenu a').click(function() {
//     $('#mobileMenu').foundation('close')
// });

/* -----------------------------------------------------------------------------
/ Work Preview
/ --------------------------------------------------------------------------- */
$('.project-header').hover(
    function() {
        id = $(this).attr('data-id');
        width = $('#link-'+id).width();
        $('#preview-'+id).css('left', width + 45).removeClass('hidden');
    },
    function() {
        $('#preview-'+id).addClass('hidden');
    }
);

/* -----------------------------------------------------------------------------
/ Menu color on scroll
/ --------------------------------------------------------------------------- */

$(window).scroll(function() {

    // get element positions
    current_position = $(window).scrollTop();
    work = $('#projects').offset().top - 215;
    contact = $('#contact').offset().top - 365;

    if(current_position >= contact) {
        new_menu = 'contact';
    } else if(current_position >= work) {
        new_menu = 'work';
    } else {
        new_menu = 'top';
    }

    $('.floating-menu a').removeClass('is-active');
    $('.floating-menu #link-'+new_menu).addClass('is-active');

});
