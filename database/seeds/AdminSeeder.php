<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (config('app.env') == 'local') {
            $develop = [
                'name' => 'Joaquín Vidal',
                'email' => 'joacovidal@gmail.com',
                'password' => bcrypt('secret'),
                'created_at' => Carbon::now()
            ];
            DB::table('users')->insert($develop);
        }
        if (config('app.env') == 'production') {
            $developer = [
                'name' => 'Joaquín Vidal',
                'email' => 'joacovidal@gmail.com',
                'password' => bcrypt('EP3DclTLOSTi'),
                'created_at' => Carbon::now()
            ];
            $client = [
                'name' => 'Amateur.Rocks',
                'email' => 'hello@amateur.rocks',
                'password' => bcrypt('dieasanamateur1980'),
                'created_at' => Carbon::now()
            ];
            DB::table('users')->insert($developer);
            DB::table('users')->insert($client);
        }
    }
}
