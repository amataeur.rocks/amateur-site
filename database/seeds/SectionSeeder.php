<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $about = '<p>Amateur(dot)rocks is a young duo of art directors and graphic designers focused on exploring concepts, simplifying (people) experiences and stimulating discussions about social issues. They understand design as a tool to generate boldness, vibrant and no-frills content for brands, projects and people.</p>
        <p>They believe that ideas are outside office walls, in the streets where real people live and the big action takes place. Amateur divide their time between commercial work — art direction, graphic design, branding experiences, creative consultancy, packaging and web design — socially committed personal projects and beers after work. Although they define themselves as a nomad duo, they are currently based in Buenos Aires.</p>';

        $contact = '<p>Amateur(dot)rocks<br>34˚35’ S, 58˚22’ W</p>
            <p>
            Work ⟶ <a href="mailto:hello@amateur.rocks">hello(at)amateur.rocks</a><br>
            Inquires ⟶ <a href="mailto:hello@amateur.rocks">hello(at)amateur.rocks</a><br>
            Press ⟶ <a href="mailto:hello@amateur.rocks">hello(at)amateur.rocks</a><br>
            Beers ⟶ <a href="mailto:hello@amateur.rocks">beers(at)amateur.rocks</a><br>
            Phone ⟶ +54 9 264 4416804<br>
            Instagram ⟶ <a target="_blank" href="https://www.instagram.com/amateurdotrocks/">@amateurdotrocks</a><br>
            Tumblr ⟶ <a target="_blank" href="http://dieasanamateur.tumblr.com/">@dieasanamateur</a><br>
            Twitter ⟶ We aren’t politicians.
            </p>';

        $get_high = '';

        $sections = [
            ['name' => 'about', 'title' => 'About', 'content' => $about, 'is_link' => false, 'created_at' => Carbon::now()],
            ['name' => 'contact', 'title' => 'Contact', 'content' => $contact, 'is_link' => false, 'created_at' => Carbon::now()],
            ['name' => 'get_high', 'title' => 'Get High', 'content' => $get_high, 'is_link' => true, 'created_at' => Carbon::now()]
        ];

        foreach ($sections as $section) {
            DB::table('sections')->insert($section);
        }
    }
}
