<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MediaTypeSeederBis extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $media_types = [
            ['code' => 'link', 'name' => 'Link', 'is_external' => false, 'external_url' => null, 'is_group' => false, 'created_at' => Carbon::now()],
            ['code' => 'vimeo', 'name' => 'Vimeo Video', 'is_external' => true, 'external_url' => 'https://vimeo.com/', 'is_group' => false, 'created_at' => Carbon::now()]
        ];

        foreach ($media_types as $type) {
            DB::table('media_types')->insert($type);
        }
    }
}
