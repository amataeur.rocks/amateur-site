<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index')->name('home');

// Helper Routes
Route::post('/project/media', 'SiteController@get_project_media');

// Auth Routes
Route::group(['middleware' => 'guest', 'prefix' => 'admin'], function() {
    Route::get('/login', 'Backend\LoginController@index')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    //Route::get('/admin/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    //Route::post('/admin/register', 'Auth\RegisterController@register');
});
Route::get('/admin/logout', 'Auth\LoginController@logout')->name('logout');

// Backend Routes
Route::group(['middleware' => 'backend', 'prefix' => 'admin'], function() {

    // Pasword Change
    Route::get('/password', 'Backend\ChangePasswordController@index')->name('password.change');
    Route::post('/password/update', 'Backend\ChangePasswordController@update')->name('password.update');

    // Dashboard Home
    Route::get('/', 'Backend\BackendController@index')->name('admin');

    // Resource Projects
    Route::resource('project', 'Backend\ProjectController');
    Route::get('project/{project}/ask', 'Backend\ProjectController@ask_delete')->name('project.ask_delete');
    Route::post('project/update_order', 'Backend\ProjectController@update_order')->name('project.update_order');

    // Resource Media
    Route::get('project/{project}/media', 'Backend\MediaController@index')->name('project.media.index');
    Route::get('project/{project}/media/create', 'Backend\MediaController@create')->name('project.media.create');
    Route::post('project/{project}/media', 'Backend\MediaController@store')->name('project.media.store');
    Route::get('project/{project}/media/{media}/edit', 'Backend\MediaController@edit')->name('project.media.edit');
    Route::match(['put', 'patch'], 'project/{project}/media/{media}', 'Backend\MediaController@update')->name('project.media.update');
    Route::get('project/{project}/media/{media}/ask', 'Backend\MediaController@ask_delete')->name('project.media.ask_delete');
    Route::delete('project/{project}/media/{media}', 'Backend\MediaController@destroy')->name('project.media.destroy');
    Route::post('project/{project}/media/update_order', 'Backend\MediaController@update_order')->name('project.media.update_order');

    // Resource Sections
    Route::resource('section', 'Backend\SectionController', ['except' => [
        'create', 'store', 'destroy'
    ]]);

});
